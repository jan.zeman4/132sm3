set term fig
set output 'figure1_m_fig.fig'

set parametric
set trange[0:1]

unset border

set noxtics
set noytics 

x12(t)=2.5*t
M12(t)=9*(1.-x12(t))

x23(t)=x12(1)+2.5*t
M23(t)=M12(1)+9*(x23(t)-x12(1))

x34(t)=x23(1)+2*t
M34(t)=M23(1)-9*(x34(t)-x23(1))

x45(t)=x34(1)+3*t
M45(t)=M34(1)+3*(x45(t)-x34(1))

plot \
     x12(t), M12(t) with filledcurves y1=0 notitle lt 1, \
     x23(t), M23(t) with filledcurves y1=0 notitle lt 1, \
     x34(t), M34(t) with filledcurves y1=0 notitle lt 1, \
     x45(t), M45(t) with filledcurves y1=0 notitle lt 1
     
#pause -1
