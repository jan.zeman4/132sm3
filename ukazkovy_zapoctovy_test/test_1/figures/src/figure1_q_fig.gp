set term fig
set output 'figure1_q_fig.fig'

set parametric
set trange[0:1]

unset border

set noxtics
set noytics 

x12(t)=2.5*t
Q12(t)=9

x23(t)=x12(1)+2.5*t
Q23(t)=-9

x34(t)=x23(1)+2*t
Q34(t)=9

x45(t)=x34(1)+3*t
Q45(t)=-3

plot \
     x12(t), Q12(t) with filledcurves y1=0 notitle lt 1, \
     x23(t), Q23(t) with filledcurves y1=0 notitle lt 1, \
     x34(t), Q34(t) with filledcurves y1=0 notitle lt 1, \
     x45(t), Q45(t) with filledcurves y1=0 notitle lt 1
     
#pause -1
