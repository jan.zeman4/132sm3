set term fig
set output 'figure2_m_fig.fig'

set size square

set parametric
set trange[0:1]

unset border

set noxtics
set noytics 

x12(t)=3*t
M12(t)=9*x12(t)

x23(t)=4*t
M23(t)=M12(1)

x34(t)=3*t
M34(t)=3*x34(t)*x34(t)

plot \
     M12(t), x12(t) with filledcurves y1=0 notitle lt 1, \
     x23(t), M23(t) with filledcurves y1=0 notitle lt 1, \
     x34(t), -M34(t) with filledcurves y1=0 notitle lt 1
     
#pause -1
