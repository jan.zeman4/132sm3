set term fig
set output 'figure2_n_fig.fig'

#set size square

set parametric
set trange[0:1]

unset border

set noxtics
set noytics 

x12(t)=3*t
N12(t)=0

x23(t)=x12(1)+4*t
N23(t)=-9

x34(t)=x23(1)+3*t
N34(t)=0

plot \
     x12(t), N12(t) with filledcurves y1=0 notitle lt 1, \
     x23(t), N23(t) with filledcurves y1=0 notitle lt 1, \
     x34(t), N34(t) with filledcurves y1=0 notitle lt 1
     
#pause -1
