set term fig
set output 'figure2_q_fig.fig'

#set size square

set parametric
set trange[0:1]

unset border

set noxtics
set noytics 

x12(t)=3*t
Q12(t)=9

x23(t)=x12(1)+4*t
Q23(t)=0

x34(t)=x23(1)+3*t
Q34(t)=-18+6*(x34(t)-x23(1))

plot \
     x12(t), Q12(t) with filledcurves y1=0 notitle lt 1, \
     x23(t), Q23(t) with filledcurves y1=0 notitle lt 1, \
     x34(t), Q34(t) with filledcurves y1=0 notitle lt 1
     
#pause -1
