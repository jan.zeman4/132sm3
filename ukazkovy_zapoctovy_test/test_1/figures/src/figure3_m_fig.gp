set term fig
set output 'figure3_m_fig.fig'

set parametric
set trange[0:1]

unset border

set noxtics
set noytics 

x12(t)=5*t
M12(t)=12-7.2*x12(t)

x23(t)=x12(1)+3*t
M23(t)=-10*(x23(t)-x23(0))

x3F(t)=x23(1)+t
M3F(t)=21+21.5*(x3F(t)-x3F(0))

xF4(t)=x3F(1)+t
MF4(t)=42.5*(1-t)

plot \
     x12(t), -M12(t) with filledcurves y1=0 notitle lt 1, \
     x23(t), -M23(t) with filledcurves y1=0 notitle lt 1, \
     x3F(t), -M3F(t) with filledcurves y1=0 notitle lt 1, \
     xF4(t), -MF4(t) with filledcurves y1=0 notitle lt 1
     
#pause -1
