set term fig
set output 'figure3_q_fig.fig'

set parametric
set trange[0:1]

unset border

set noxtics
set noytics 

x12(t)=5*t
Q12(t)=-7.2

x23(t)=x12(1)+3*t
Q23(t)=-10

x3F(t)=x23(1)+t
Q3F(t)=21.5

xF4(t)=x3F(1)+t
QF4(t)=-42.5

plot \
     x12(t), Q12(t) with filledcurves y1=0 notitle lt 1, \
     x23(t), Q23(t) with filledcurves y1=0 notitle lt 1, \
     x3F(t), Q3F(t) with filledcurves y1=0 notitle lt 1, \
     xF4(t), QF4(t) with filledcurves y1=0 notitle lt 1
     
#pause -1
