\documentclass[12pt]{article}
\usepackage{graphicx}
\usepackage[czech]{babel}
\usepackage{fullpage}
\usepackage{multicol}
\usepackage{times}
\usepackage[pdfpagemode=None,colorlinks,citecolor=red,urlcolor=red]{hyperref}
\usepackage{amssymb}
\usepackage{hyperref}

\newcommand{\seprule}{\rule{\textwidth}{1.5pt}}

\input{macros}

\pagestyle{empty}

\begin{document}

\noindent

\paragraph{Příklad 1} 
%%%%%%%%%%%%%%%%%%%%%
Určete průběhy ohybových momentů $\M$ a posouvajících sil $\V$ na dané
konstrukci.

\centerline{
 \includegraphics[width=.6\textwidth]{figures/figure1}
}

\paragraph{Řešení}

Řešená konstrukce je staticky určitá, jedná se o tzv. \emph{Gerberův
  nosník}. Při určení reakcí s výhodou využijeme skutečnosti, že
  jednotlivé pruty můžeme \uv{hierarchicky} rozdělit na nesené a
  nesoucí části, jak je naznačeno na následujícím schématu.

\centerline{
 \includegraphics[width=.6\textwidth]{figures/figure1_decomp}
}

Reakce ve vnějších a vnitřních vazbách nyní můžeme řešit postupem
\uv{shora dolů}. Nejprve určíme reakce v nejvýše položeném
nosníku~\nosnik{II}~($B = C = 9$~kN), pak pokračujeme
částmi~\nosnik{I}~($M_A = -9$~kNm, $A=9$~kN)
a~\nosnik{III}~($E=-9$~kN, $D = 18$~kN) a nakonec vyřešíme nosník
\nosnik{IV}~($G = 3$~kN, $F=-12$~kN). Reakce zkontrolujeme podmínkou
rovnováhy celé konstrukce:
%
\begin{eqnarray*}
\nosnik{I+II+III+IV}:\stackrel{\curvearrowleft}{A}: & 
-M_A-18 \cdot 2,5 + D \cdot 5 + F \cdot 7 + G \cdot 10 = 9 - 45 + 90 -
84 + 30 = 0\mbox{ Nm}
\end{eqnarray*}
%
Známé hodnoty reakcí využijeme k vykreslení průběhu vnitřních sil.

\centerline{
\begin{tabular}{cc}
\framebox{$\M$~[kNm]} \\ 
& \includegraphics[width=.555\textwidth]{figures/figure1_m} \\
\framebox{$\V$~[kN]} \\
& \includegraphics[width=.555\textwidth]{figures/figure1_q} 
\end{tabular}
}
%
\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\paragraph{Příklad 2} 
%%%%%%%%%%%%%%%%%%%%%
Určete průběhy momentů $\M$, normálových sil $\N$ a posouvajících sil
$\V$ na dané konstrukci.

\begin{minipage}[c]{.45\textwidth}
 \includegraphics{figures/figure2}
\end{minipage}
%
\hfill
%
\begin{minipage}[c]{.45\textwidth}
 \includegraphics[width=.9\textwidth]{figures/figure2_reakce}
\end{minipage}

\paragraph{Řešení} Konstrukce je staticky určitá, začneme
tedy výpočtem reakcí
%
\begin{eqnarray*}
\uparrow: && B_z = 0\mbox{ N} \\
\stackrel{\curvearrowleft}{B}: && 18 \cdot 1,5 - A \cdot 3 = 0
\Rightarrow A = 9\mbox{ kN} \\
\rightarrow: && A + B_x - 18 = 0 \Rightarrow B_x = 9\mbox{ kN}
\end{eqnarray*}
%
a příklad zakončíme vykreslením vnitřních sil.

\begin{center}
\begin{tabular}{cc}
\framebox{$\M$~[kNm]} & \framebox{$\N$~[kN]} \\
\includegraphics[height=50mm]{figures/figure2_m} &
\includegraphics[height=50mm]{figures/figure2_n} \\[5mm]
\multicolumn{2}{c}{\framebox{$\V$~[kN]}} \\
\multicolumn{2}{c}{\includegraphics[height=50mm]{figures/figure2_q}}
\end{tabular}
\end{center}
%
\newpage

\paragraph{Příklad 3} Zjednodušenou deformační metodou určete průběh
vnitřních sil na konstrukci. Dále určete průhyb uprostřed vodorovného
prutu. $EI = \mbox{konst} = 30$~MNm$^2$.

\begin{center}
 \begin{minipage}[c]{.45\textwidth}
   \includegraphics{figures/figure3} 
 \end{minipage}
%
 \begin{minipage}[c]{.45\textwidth}
   \includegraphics[height=55mm]{figures/figure3_nezname} 
 \end{minipage}
\end{center}

\paragraph{Řešení} zorganizujeme obdobně jako v přednáškových materiálech.

\begin{enumerate}

\item Očíslujeme pruty a styčníky a určíme ohybové tuhosti
  jednotlivých prutů.
%
\begin{eqnarray*}
k_{13} & = & \frac{2EI}{\ell_{13}} 
= \frac{2 \cdot 30}{5} 
= 12\mbox{~MNm} \\
k_{23} & = & \frac{2EI}{\ell_{23}} 
= \frac{2 \cdot 30}{3} 
= 20\mbox{~MNm} \\
k_{34} & = & \frac{2EI}{\ell_{34}} 
= \frac{2 \cdot 30}{2} 
= 30\mbox{~MNm} 
\end{eqnarray*}

\item Zavedeme základní deformační neznámé. Jelikož budeme prut $23$
  uvažovat jako typ \uv{kloub--vetknutí} a prut $34$ jako typ
  \uv{vetknutí--kloub}, má daná úloha za předpokladů zjednodušené
  deformační metody jednu základní neznámou, a to pootočení
  styčníku~\uzel{3}:~$\reaction{\varphi_3}$.

\item Sestavíme styčníkové podmínky rovnováhy, pro každou deformační
  neznámou jednu.

\centerline{
 \includegraphics{figures/figure3_stycnik}
}

$$
\stackrel{\curvearrowright}{\uzel{3}}: 
\defunk{M_{31}} + \defunk{M_{32}} + \defunk{M_{34}} + \eload{75} = 0\mbox{~kNm}
$$

\item Vyjádříme jednotlivé koncové momenty jako funkce deformačních
  neznámých~(momentu a tuhosti jsou uvedeny Nm):
%
\begin{eqnarray*}
M_{13} & = & M_{13}^{(f)} + M_{13}^{(\varphi)} 
= 0 + k_{13}( 2 \varphi_1 + \varphi_3 ) = 12 \cdot 10^{6} \varphi_3 \\
M_{31} & = & M_{31}^{(f)} + M_{31}^{(\varphi)} 
= 0 + k_{13}( \varphi_1 + 2\varphi_3 ) = 24 \cdot 10^{6} \varphi_3 \\
M_{23} & = & 0 \\
M_{32} & = & M_{32}^{(f,KV)} + M_{32}^{(\varphi,KV)}
= 0 + k_{23} ( \frac{3}{2} \varphi_3 ) = 30 \cdot 10^{6} \varphi_3 \\
M_{34} & = & M_{34}^{(f,VK)} + M_{34}^{(\varphi,VK)}
= \frac{3}{16} F \ell_{34} + k_{34} ( \frac{3}{2} \varphi_3 )
= \frac{3}{16} 64 \cdot 10^3 \cdot 2 + 45\cdot 10^6 \varphi_3 \\
& = & 24 \cdot 10^3 + 45 \cdot 10^6 \varphi_3 \\
M_{43} & = & 0
\end{eqnarray*}

\item Dosadíme koncové momenty do styčníkových podmínek rovnováhy
%
\begin{eqnarray*}
24 \cdot 10^{6} \varphi_3 + 
30 \cdot 10^{6} \varphi_3 + 
24 \cdot 10^3 + 45 \cdot 10^6 \varphi_3 +
75 \cdot 10^3 & = & 0 \\
99 \cdot 10^6 \varphi_3 & = & -99 \cdot 10^3
\end{eqnarray*}

\item Vyřešíme předchozí rovnici:~$\varphi_3 = - 1 \cdot 10^{-3}$~rad

\item Dosadíme známou hodnotu pootočení $\varphi_3$ do výrazů pro
  koncové momenty sestavené ve 4. kroku:
%
\begin{eqnarray*}
M_{13} = -12\mbox{~kNm} && M_{31} = -24\mbox{~kNm} \\
M_{23} = 0\mbox{~Nm} && M_{32} = -30\mbox{~kNm} \\
M_{34} = 24 - 45 = - 21\mbox{~kNm} &&
M_{43} = 0\mbox{~Nm}
\end{eqnarray*} 

\item Určíme reakce na jednotlivých prutech \emph{odděleně}

\centerline{
  \includegraphics{figures/figure3_reakce}
}

\begin{eqnarray*}
13: \stackrel{\curvearrowleft}{A}: && 
5 \cdot B - 24 - 12 = 0 \Rightarrow B = 7,2\mbox{~kN} \\ 
13: \uparrow: && 
A + B = 0 \Rightarrow A = -7,2 \mbox{~kN} \\
23: \stackrel{\curvearrowright}{D}: && 
30 + 3 \cdot C = 0 \Rightarrow C = -10\mbox{~kN} \\
23: \leftarrow: && 
C + D = 0 \Rightarrow D = 10\mbox{~kN} \\
34: \stackrel{\curvearrowright}{E}: && 
21 + 1 \cdot 64 - 2 \cdot F  = 0 \Rightarrow 
F = 42,5\mbox{~kN} \\
34: \rightarrow: && 
64 - E - F = 0 \Rightarrow 
E = 21,5\mbox{~kN} 
\end{eqnarray*}


\item Vykreslíme průběh momentů $\M$ a posouvajících sil $\V$ na dané
  konstrukci \uv{složením} průběhů na jednotlivých prutech.

\begin{tabular}{cc}
\framebox{$\M$~[kNm]} & \framebox{$\V$~[kN]} \\
\includegraphics[width=.425\textwidth]{figures/figure3_m} &
\includegraphics[width=.375\textwidth]{figures/figure3_q}
\end{tabular}

\item Průběh normálových sil získáme ze styčníkových podmínek
  rovnováhy a použitím rovnosti $N_{32}=N_{23} = 0$~N.

\centerline{
 \includegraphics{figures/figure3_stycnik_nq}
}

\begin{eqnarray*}
\uzel{3}:\leftarrow: && -10 - 21,5 + N_{31} = 0 
\Rightarrow N_{31} = 31,5\mbox{~kN} \\
\uzel{3}:\uparrow: && -7,2 + N_{34} = 0 
\Rightarrow N_{34} = 7,2\mbox{~kN}
\end{eqnarray*}

\newpage
Nyní můžeme určit průběh normálových sil po konstrukci.
%
\begin{center}
\framebox{$N$~[kN]}\\
\includegraphics{figures/figure3_n}
\end{center}

\item Hodnoty vypočtených vnitřních sil zkontrolujeme podmínkami
  rovnováhy na celé konstrukci:

\centerline{%
 \includegraphics{figures/figure3_kontrola}
}

\begin{eqnarray*}
\rightarrow: && -31,5 -42,5 + 10 + 64 = 0 \\
\uparrow: && -7,2 + 7,2 = 0 \\
\stackrel{\curvearrowleft}{\uzel{1}}: && 
-12 -75 - 64 \cdot 1 + 7,2 \cdot 5 + 42,5 \cdot 2 + 10 \cdot 3 = 0
\end{eqnarray*}

\item Na závěr přistoupíme k výpočtu průhybu:
%
\begin{eqnarray*}
w_{13}( \xi = 0,5 ) & = & 
w_{13}^{(f)}( \xi = 0,5 ) + w_{13}^{(\varphi)}( \xi = 0,5 )
= 0 
- \varphi_1 \ell_{13} \xi( \xi - 1 )^2 
- \varphi_3 \ell_{13} \xi^2( \xi - 1 ) \\
& = & 10^{-3} \cdot 5 \cdot 0,25 \cdot( 0,5 - 1 ) = 
- 6,25 \cdot 10^{-4}\mbox{ m}
\end{eqnarray*}

Alternativně můžeme určit hodnotu průhybu integrací konstitutivní
rovnice pro ohyb~(\uv{integrace ohybové čáry})
%
\begin{eqnarray*}
-EI w''_{13}(x) & = & M_{13}(x) = 12 - 7,2 x \\
-EI w'_{13}(x)   & = & 12 x - 3,6 x^2 + C_1 
\hspace{1cm}
[w'_{13}( x = 0 ) = 0 \Rightarrow C_1 = 0]\\
-EI w_{13}(x)   & = & 6 x^2 - 1,2 x^3 + C_2
\hspace{1cm}
[w_{13}( x = 0 ) = 0 \Rightarrow C_2 = 0],
\end{eqnarray*}
%
kde pravá strana poslední rovnosti vyjde v kNm, dosadíme-li $x$ v
metrech. Pro průhyb uprostřed nosníku $13$ tedy platí
%
$$
w_{13}( x = 2,5\mbox{ m} ) = 
-\frac{6 \cdot 2,5^2 - 1,2 \cdot 2,5^3}{30 \cdot 10^3}
=-6,25 \cdot 10^{-4}\mbox{ m}
$$

\end{enumerate}

\plea

\end{document}