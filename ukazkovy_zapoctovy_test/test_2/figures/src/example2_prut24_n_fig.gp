set term fig
set output 'example2_fig24_n_fig.fig'

set parametric
set trange[0:1]

unset border

set noxtics
set noytics 

x24(t)=6*t
N24(t)=-31.2

plot \
     x24(t), N24(t) with filledcurves y1=0 notitle lt 1
     
#pause -1
