set term fig
set output 'example2_fig24_m_fig.fig'

set parametric
set trange[0:1]

unset border

set noxtics
set noytics 

x24(t)=6*t
M24(x)=-8.53+51.62*x-10*x*x

plot \
     x24(t), M24(x24(t)) with filledcurves y1=0 notitle lt 1
     
#pause -1
