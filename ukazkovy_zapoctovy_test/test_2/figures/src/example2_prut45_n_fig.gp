set term fig
set output 'example2_fig45_n_fig.fig'

set parametric
set trange[0:1]

unset border

set noxtics
set noytics 

x45(t)=3*t
N45(t)=39.07

plot \
     x45(t), N45(t) with filledcurves y1=0 notitle lt 1
     
#pause -1
