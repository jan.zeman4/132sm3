set term fig
set output 'example3_m_fig.fig'

set parametric
set trange[0:1]

unset border

set noxtics
set noytics 

x12(t)=4*t
M12(t)=48.85*(1-t)-21.60*t

x24(t)=6*t
M24(t)=21.60*(1-t)+11.26*t

x34(t)=4*t
M34(t)=116.7*(1-t)+13.37*t

x45(t)=3*t
M45(t)=24.63*(1-t)

plot \
     x12(t), M12(t) with filledcurves y1=0 notitle lt 1, \
     x24(t), M24(t) with filledcurves y1=0 notitle lt 1, \
     x34(t), M34(t) with filledcurves y1=0 notitle lt 1, \
     x45(t), M45(t) with filledcurves y1=0 notitle lt 1
     
#pause -1
