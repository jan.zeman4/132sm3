set term fig
set output 'example3_n_fig.fig'

set parametric
set trange[0:1]

unset border

set noxtics
set noytics 

x12(t)=4*t
N12(t)=1.72

x24(t)=6*t
N24(t)=17.61

x34(t)=4*t
N34(t)=0.74

x45(t)=3*t
N45(t)=0.89

plot \
     x12(t), N12(t) with filledcurves y1=0 notitle lt 1, \
     x24(t), N24(t) with filledcurves y1=0 notitle lt 1, \
     x34(t), N34(t) with filledcurves y1=0 notitle lt 1, \
     x45(t), N45(t) with filledcurves y1=0 notitle lt 1
     
#pause -1
